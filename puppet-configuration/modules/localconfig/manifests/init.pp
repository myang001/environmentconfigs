class localconfig {

  $rds_jdbc = hiera('rds_jdbc')
  $rds_user = hiera('rds_user')
  $rds_pass = hiera('rds_pass')
  $aws_accessKeyId = hiera('aws_accessKeyId')
  $aws_secretKey = hiera('aws_secretKey')

  package { "git": }
  package { "augeas": }

file { "/root/.aws":
    ensure => 'directory'
  }
file { 'awscli-config':
    path => '/root/.aws/config',
    ensure => 'file',
    content => "[default]
    output = json
    region = us-east-1
    aws_access_key_id =  $aws_accessKeyId
    aws_secret_access_key = $aws_secretKey"
  }

exec { "download mysql repo":
  creates => "/tmp/download-cache/mysql-community-release-el6-5.noarch.rpm",
  command => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/mysql-community-release-el6-5.noarch.rpm /tmp/download-cache",
  user => 'root',
  timeout => 0,
  require => [Package['awscli'], File['awscli-config']]
}
-> package { 'mysql-community-release':
  ensure => installed,
  source => '/tmp/download-cache/mysql-community-release-el6-5.noarch.rpm',
  provider => rpm
}
-> package { 'mysql-community-server':
  ensure => latest
}
-> class { '::mysql::server':
  root_password => 'vagrant',
  override_options => {
    mysqld => {
      "default-storage-engine" => "INNODB",
      "lower_case_table_names" => "1",
      "bind_address" => "0.0.0.0"
    }
  }
}
-> class { '::mysql::client': 
}
-> mysql_database { 'test' :
  ensure => 'present'
}
-> mysql_database { 'cdta' :
  ensure => 'present'
}
-> mysql_user { 'genfare@%' :
  ensure => 'present',
  password_hash => mysql_password('genfarerds'),
  provider      => mysql
}
-> mysql_user { 'genfare@localhost' :
  ensure => 'present',
  password_hash => mysql_password('genfarerds'),
  provider      => mysql
}
-> mysql_grant { 'genfare@%/*.*':
  ensure => 'present',
  options => ['GRANT'],
  privileges => ['ALL'],
  user       => 'genfare@%',
  table      => '*.*',
  provider   => mysql
}
-> mysql_grant { 'genfare@localhost/*.*':
  ensure => 'present',
  options => ['GRANT'],
  privileges => ['ALL'],
  user       => 'genfare@localhost',
  table      => '*.*',
  provider   => mysql
}


}
