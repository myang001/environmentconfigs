class puppetmasterconfig {

<<<<<<< HEAD
    class { '::ntp': }

    vcsrepo { '/etc/puppet/.env/dev':
      ensure   => latest,
      provider => git,
      source   => 'ssh://git@altssh.bitbucket.org:443/genfare/environmentconfigs.git',
      revision => 'dev'
    } -> file { '/etc/puppet/environments/dev': ensure => 'link', target => '/etc/puppet/.env/dev/puppet-configuration' }

    vcsrepo { '/etc/puppet/.env/intg':
      ensure   => latest,
      provider => git,
      source   => 'ssh://git@altssh.bitbucket.org:443/genfare/environmentconfigs.git',
      revision => 'intg'
    } -> file { '/etc/puppet/environments/intg': ensure => 'link', target => '/etc/puppet/.env/intg/puppet-configuration' }

    vcsrepo { '/etc/puppet/.env/qa':
=======
    #Clone and continue to pull branches for environments
    vcsrepo { '/etc/puppet/environments/dev':
      ensure   => latest,
      provider => git,
      source   => 'git@bitbucket.org:genfare/environmentconfigs.git',
      revision => 'dev'
    }
    vcsrepo { '/etc/puppet/environments/intg':
      ensure   => latest,
      provider => git,
      source   => 'git@bitbucket.org:genfare/environmentconfigs.git',
      revision => 'intg'
    }
    vcsrepo { '/etc/puppet/environments/qa':
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
      ensure   => latest,
      provider => git,
      source   => 'ssh://git@altssh.bitbucket.org:443/genfare/environmentconfigs.git',
      revision => 'qa'
<<<<<<< HEAD
    } -> file { '/etc/puppet/environments/qa': ensure => 'link',target => '/etc/puppet/.env/qa/puppet-configuration' }

    vcsrepo { '/etc/puppet/.env/uat':
      ensure   => latest,
      provider => git,
      source   => 'ssh://git@altssh.bitbucket.org:443/genfare/environmentconfigs.git',
      revision => 'uat'
    } -> file { '/etc/puppet/environments/uat': ensure => 'link', target => '/etc/puppet/.env/uat/puppet-configuration' }

    vcsrepo { '/etc/puppet/.env/production':
=======
    }
    vcsrepo { '/etc/puppet/environments/uat':
      ensure   => latest,
      provider => git,
      source   => 'git@bitbucket.org:genfare/environmentconfigs.git',
      revision => 'uat'
    }
    vcsrepo { '/etc/puppet/environments/production':
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
      ensure   => latest,
      provider => git,
      source   => 'ssh://git@altssh.bitbucket.org:443/genfare/environmentconfigs.git',
      revision => 'master'
    } -> file { '/etc/puppet/environments/production': ensure => 'link', target => '/etc/puppet/.env/production/puppet-configuration' }

    firewall { '100 allow puppetmaster access':
      port   => 8140,
      proto  => tcp,
      action => accept,
    }

    tidy { "/var/lib/puppet/reports":
      age => "1w",
      recurse => true,
    }

    service { "puppetmaster":
      enable => true,
    }
}
