define usermgmt::type::authed_user ( 
  $userhash=undef 
  ){
  $username=$title
  $pubkey=$userhash[$username][pubkey]
  $hashedpass=$userhash[$username][hashedpassword]
  user { $username :
    ensure     => present,
    home       => "/home/${username}",
    password   => $hashedpass,
    groups     => 'admins',
    managehome => true
  }
                          
  file {"/home/${username}/.ssh" :
    ensure  => directory,
    owner   => $username,
    require => User[$username]
  }

  ssh_authorized_key { "${username}_key" :
    type    => 'ssh-rsa',
    user    => $username,
    ensure  => present,
    key     => $pubkey,
    require => File["/home/${username}/.ssh"]
  }
}
