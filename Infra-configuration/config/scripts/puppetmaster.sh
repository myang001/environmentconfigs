#!/bin/bash

usage() { echo "Usage: $0 -b bucketname" 1>&2; exit 1; }

while getopts ":b:e:" o; do
    case "${o}" in
        b)
            bucket=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${bucket}" ]; then
    usage
fi

#update and nstall needed packages
yum update -y
yum install -y https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-7.noarch.rpm
yum install -y puppet-3.4.2-1.el6.noarch puppet-server-3.4.2-1.el6.noarch ruby-devel gcc make rubygems git
gem install json

#Setup git client
python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('keys/id_rsa.pub').get_contents_to_filename('/root/.ssh/id_rsa.pub')"
python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('keys/id_rsa').get_contents_to_filename('/root/.ssh/id_rsa')"

chmod 644 /root/.ssh/id_rsa.pub
chmod 600 /root/.ssh/id_rsa
ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts

#Setup puppet master
mkdir -p /etc/puppet/environments/

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/autosign.conf').get_contents_to_filename('/etc/puppet/autosign.conf')"
chmod 644 /etc/puppet/autosign.conf

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/fileserver.conf').get_contents_to_filename('/etc/puppet/fileserver.conf')"
chmod 644 /etc/puppet/fileserver.conf

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/puppet.conf').get_contents_to_filename('/etc/puppet/puppet.conf')"
chmod 644 /etc/puppet/puppet.conf

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/hiera.yaml').get_contents_to_filename('/etc/puppet/hiera.yaml')"
chmod 644 /etc/puppet/hiera.yaml

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/auth.conf').get_contents_to_filename('/etc/puppet/auth.conf')"
chmod 644 /etc/puppet/auth.conf

python -c "import
boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem').get_contents_to_filename('/var/lib/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem')"
chmod 640 /var/lib/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/ssl/ca/signed/generic-hostcert.gfcp.io.pem').get_contents_to_filename('/var/lib/puppet/ssl/ca/signed/generic-hostcert.gfcp.io.pem')"
chmod 640 /var/lib/puppet/ssl/ca/signed/generic-hostcert.gfcp.io.pem

prod_path="/etc/puppet/.env/production"
if [ -d "$prod_path" ]; then
   cd $prod_path && git checkout master && git pull origin master
else
   git clone git@bitbucket.org:genfare/environmentconfigs.git $prod_path
   ln -s $prod_path/puppet-configuration /etc/puppet/environments/production
fi

chkconfig puppetmaster on --level 2345

puppetdns=`hostname --fqdn`

eval "sed -i 's/%PuppetMaster%/$puppetdns/' /etc/puppet/puppet.conf"

echo '{ "Puppet" : { "role" : "puppetmaster"}}' > /root/puppet_facts.json

service puppetmaster restart

service puppet restart
