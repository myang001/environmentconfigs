# Class: openvpnconfig
#
# This module manages the configuration of an openvpn access server aws ami
# So the server is already installed we are just managing the configuration
#
# The files that are copied onto the host were compiled on another openvpn 
# ami image, and are just being hosted in puppet and copied out so we do not
# need all of the libraries to compile and dont need to do that within puppet.
# In the future these should be hosted in out Pulp/Satellite repo as an rpm.
#
# Hiera:
#   OTPEnabled - determines if the openvpn server should be configured to 
#   require the use of a One Time Password, from Google Authenticator, to 
#   login to the vpn server.  If false, which is the default, then the setup
#   is pam auth which just uses the hosts users and credentials.
#
#   openvpn_admins - This is needed if using OTPEnabled, if not you will not be able
#   to access the admin console since you can not setup OTP for the admin 
#   user that was specified in the openvpn cloudformations template.  This is
#   just a list of users that can log into the admin console .
#
# Actions:
#
# Requires: User management to be complete, in our case Class['usermgmt']
#
# Sample Usage:
#
# node default {
#   class { 'openvpnconfig': }
# }
# 
# In hiera
#vpn_client_net: 172.30.0.0
#vpn_client_netmask: 24
#vpn_routing_subnets:
#  - 172.31.0.0/16
#openvpn_admins:
#  - bmadaras
#openvpn_OTPEnabled: false
#
class openvpnconfig {
    include openvpnconfig::files

    $openvpn_root = '/usr/local/openvpn_as'


    #The configuration for the as.conf file is being done as adding file lines because this
    #file is created using values from the cloudform template such as the external ip which is
    #not available as one of puppets facts and does not require heavy modification
    file_line { 'add_pam_plugin':
        path   => "${openvpn_root}/etc/as.conf",
        line   => 'plugin /usr/local/openvpn_as/lib/openvpn-auth-pam.so /etc/pam.d/openvpnas',
        notify => Service['openvpnas']
    }

    file_line { 'log_to_syslog':
        path   => "${openvpn_root}/etc/as.conf",
        line   => 'SYSLOG=true',
        notify => Service['openvpnas']
    }

    service { 'openvpnas':
        ensure    => running,
        enable    => true,
        hasstatus  => false
    }

    $OTPEnabled  = hiera('openvpn_OTPEnabled')
    notify {"OTPEnabled is ${OTPEnabled}":}
    file { '/etc/pam.d/openvpnas' :
        content => template('openvpnconfig/openvpnas.erb'),
        mode   => '0644',
    }

    $client_net = hiera('vpn_client_net')
    $client_mask = hiera('vpn_client_netmask')
    

    exec { "client_network" :
        command => "${openvpnconfig::openvpn_root}/scripts/confdba --mod --key=vpn.daemon.0.client.network --value=${client_net}",
        unless  => "${openvpnconfig::openvpn_root}/scripts/confdba --get --key=vpn.daemon.0.client.network | grep ${client_net}",
        notify => Service['openvpnas']
    }
    
    exec { "client_network_mask" :
        command => "${openvpnconfig::openvpn_root}/scripts/confdba --mod --key=vpn.daemon.0.client.netmask_bits --value=${client_mask}",
        unless  => "${openvpnconfig::openvpn_root}/scripts/confdba --get --key=vpn.daemon.0.client.netmask_bits | grep ${client_mask}",
        notify => Service['openvpnas']
    }
    
    exec { "auth_module" :
        command => "${openvpnconfig::openvpn_root}/scripts/confdba --mod --key=auth.module.type --value=pam",
        unless  => "${openvpnconfig::openvpn_root}/scripts/confdba --get --key=auth.module.type | grep pam",
        notify => Service['openvpnas']
    }

    $openvpn_admins = hiera('openvpn_admins')
    openvpnconfig::type::add_admin { $openvpn_admins : 
        require => Service['openvpnas']
    }

    $private_routes = hiera_array('vpn_routing_subnets')
    openvpnconfig::type::add_private_routes { $private_routes : 
        require => Service['openvpnas']
    }
}
