# Class: etlconfig
#
# This module manages etlconfig
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class etlconfig {

  package { "git": }
  package { "multitail": }
  package { "mysql-client": }

  file { "/etc/motd":
    content => "Welcome to the GenfareCloud ETL Environment. Do not make changes locally."
  }

  user { 'etladmin':
    ensure => present,
    managehome => true,
    groups => admins
  }

  class { '::epel': }
  -> package { "python-pip": }
  -> exec { "Fix pip on Fedora <= 17":       #Older fedora based systems don't have a pip symlink created by the package
  command => '/usr/bin/python-pip install -U pip',
  creates  => '/usr/bin/pip'
  }
  -> package { "awscli":
    ensure   => '1.3.11',
    provider => "pip"
  }
  -> file { "/home/vagrant/.aws":
    require => User['vagrant'],
    ensure => directory,
    owner => vagrant
  }
  -> file { 'awscli-config':
    path => '/home/vagrant/.aws/config',
    ensure => file,
    owner => vagrant,
    content => "[default]
    output = json
    region = us-east-1
    aws_access_key_id = AKIAIWKBYQKJHCGJ7LTQ
    aws_secret_access_key = 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"
  }
  -> file { "/root/.aws":
    ensure => directory
  }
  -> file { 'root awscli-config':
    path => '/root/.aws/config',
    ensure => file,
    content => "[default]
    output = json
    region = us-east-1
    aws_access_key_id = AKIAIWKBYQKJHCGJ7LTQ
    aws_secret_access_key = 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"
  }

  service { "iptables":
    ensure => stopped
  }

  $genfarenexus = {
    id => "genfarenexus",
    username => "deployment",
    password => "2th3G3nf@reCloud",
    url => "http://172.31.254.224:8081/nexus/content/groups/Genfare/",
    releases => {enabled => true},
    snapshots => {enabled => true}
  }

  class { "maven::maven": }

  Maven {
    user => vagrant
  }

  maven::settings { 'maven-user-settings' :
    repos => [$genfarenexus],
    servers => [$genfarenexus],
    user    => 'vagrant'
  }

  exec { "download jboss":
    creates => "/var/tmp/jboss-eap-6.2.0.zip",
    command => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/jboss-eap-6.2.0.zip /var/tmp",
    timeout => 0,
    require => [Package['awscli'], File['root awscli-config']],
    before => Class['jboss::install']
  }

  package { "java-1.7.0-openjdk-devel" :
    ensure => installed
  }
  -> class { 'jboss':
    install => 'source',
    install_source => 'file://tmp/jboss-eap-6.2.0.zip',
    created_dirname => 'jboss-eap-6.2',
    version => '7',
    bindaddr => '0.0.0.0',
    bindaddr_admin_console => '0.0.0.0'
  }
  -> file { "/opt/jboss/standalone/configuration/mgmt-users.properties":
    content => "admin=fef4a2d5399b026a4f9e26aa72447eb7",
    owner => jboss
  }
  -> file { ["/opt/jboss/modules/com","/opt/jboss/modules/com/mysql","/opt/jboss/modules/com/mysql/main"]:
    ensure => directory,
    owner => jboss,
    mode => 777
  }
  -> maven { "/opt/jboss/modules/com/mysql/main/mysql-connector-java-5.1.29.jar":
    groupid => "mysql",
    artifactid => "mysql-connector-java",
    version => "5.1.29",
    packaging => "jar",
    require => Class['maven::maven']
  }
  -> file { "/opt/jboss/modules/com/mysql/main/module.xml":
    owner => jboss,
    content => '<?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.0" name="com.mysql">
    <resources>
    <resource-root path="mysql-connector-java-5.1.29.jar"/>
    </resources>
    <dependencies>
    <module name="javax.api"/>
    <module name="javax.transaction.api"/>
    </dependencies>
    </module>'
  }
  -> exec { "quartz module dir":
    command => "/bin/mkdir -p /opt/jboss/modules/org/quartz/main/"
  }
  -> exec { "quartz module.xml":
    command => "/bin/cp /vagrant/vagrant/files/module.xml /opt/jboss/modules/org/quartz/main/"
  }
  -> exec { "quartz module jar":
    command => "/bin/cp /vagrant/vagrant/files/quartz-all-1.8.6.jar /opt/jboss/modules/org/quartz/main/"
  }
  -> file { "jboss-variables":
    path    => '/etc/default/jboss',
    ensure  => 'file',
    content => "JBOSS_CONFIG=standalone-ha.xml\nJBOSS_ADDITIONAL_OPTIONS='-Djboss.node.name=node1'",
  }
  -> file { '/opt/jboss/bin/standalone.conf':
    source  => '/vagrant/vagrant/files/standalone.conf',
    owner   => 'jboss',
    group   => 'jboss',
    mode    => '0755',
  }
  -> exec { 'jboss-restart':
    command => '/sbin/service jboss restart',
  }
  -> file {"/tmp/jboss.cli":
      source => "puppet:///modules/democonfig/jboss.cli"
  }
  -> exec { "configure-eap":
    command => '/opt/jboss/bin/jboss-cli.sh --connect --user=admin --password=password --file=/tmp/jboss.cli',
    require => Service['jboss'],
  }

  file { "/opt/jboss/standalone/deployments":
    ensure => directory,
    mode => 777,
    require => Class['jboss::install']
  }

  class { 'apache':
    default_vhost => false,
  }
  class { 'mod_cluster':
    require => Class['apache'],
  }

  service { 'network':
    ensure  => 'running',
  }

  if $ec2_security_groups =~ /elasticbamboo/ {
    #For some reason the /opt/jboss folder is being created in this env before the link is, causing a failure
    File <| title == 'jboss_link' |> {
    force => true
    }

    file_line { 'Set JAVA_HOME for jboss':
      path    => '/opt/jboss/bin/standalone.conf',
      line    => 'JAVA_HOME="/opt/jdk-7"',
      require => Class['jboss::install'],
      notify  => Service['jboss']
    }
  }
}
