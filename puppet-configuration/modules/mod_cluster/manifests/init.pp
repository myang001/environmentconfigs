class mod_cluster (
  $package_ensure = 'installed',
  $conf_dir       = $::apache::params::confd_dir,
  $conf_name      = 'mod_cluster'
) inherits ::apache::params {

  package { 'mod_cluster':
    ensure  => $package_ensure,
    require => Package['httpd']
  }

  file { "mod_proxy.load":
    ensure  => file,
    path    => "${conf_dir}/mod_proxy.load",
    content => template('mod_cluster/mod_proxy.load.erb'),
    require => Package['mod_cluster']
  }

  file { "mod_cluster.conf":
    ensure  => file,
    path    => "${conf_dir}/${conf_name}.conf",
    content => template('mod_cluster/mod_cluster.conf.erb'),
    require => File['mod_proxy.load'],
    notify  => Class['Apache::Service']
  }
}


