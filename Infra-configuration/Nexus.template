{
    "AWSTemplateFormatVersion" : "2010-09-09",
    "Description": "Template to standup the nexus repository within a vpc private subnet",

    "Parameters" : {
        "KeyName": {
            "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the Stack",
            "Type": "String",
            "MinLength": "1",
            "MaxLength": "255",
            "Default" : "genfareaws201403",
            "AllowedPattern" : "[\\x20-\\x7E]*",
            "ConstraintDescription" : "can contain only ASCII characters."
        },
        "NexusPrivateIP" : {
            "Description" : "The IP address to use for the Nexus repository",
            "Type": "String",
            "Default" : "172.31.254.224",
            "MinLength": "7",
            "MaxLength": "15",
            "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})",
            "ConstraintDescription": "must be a valid IP of the form x.x.x.x"
        },
        "NexusSecGroupId" : {
            "Type" : "String",
            "Description" : "The id of the Nexus Security Group the Virtual Private Cloud (VPC)"
        },
        "PrivateSubnetId" : {
            "Type" : "String",
            "Description" : "SubnetId of the private subnet in the Virtual Private Cloud (VPC)"
        },
        "PuppetMasterDNS" : {
            "Description" : "The host address for the node to connect to as the puppet master",
            "Type" : "String"
        },
        "Environment" : {
            "Description" : "The Environment that this template will be created in.",
            "Type" : "String",
            "Default" : "dev",
            "AllowedValues" : [ "dev", "qa", "production"]
        },
        "ConfigBucket" : {
            "Description" : "The bucket that contains the templates and other configuration artifacts",
            "Type" : "String",
            "Default" : "configuration-artifacts"
        },
        "ReadOnlyProfileId" : {
            "Description" : "The id of the IAM profile to be used for Read Access",
            "Type" : "String"
        }
    },
    "Resources" : {  
        "NexusRepo" : {
            "Type" : "AWS::CloudFormation::Stack",
            "Properties" : {
                "TemplateURL" : {"Fn::Join": [ "", ["https://s3.amazonaws.com/",{ "Ref": "ConfigBucket" },"/GenericHostCustomEIN.template"]]},
                "TimeoutInMinutes" : "40",
                "Parameters" : {
                    "InstanceName" : "NexusRepo",
                    "KeyName" : { "Ref" : "KeyName" } ,
                    "InstanceType" : "m1.medium",
                    "NetworkInterface" : { "Ref" : "NexusNetworkInterface"},
                    "IamInstanceProfile": { "Ref": "ReadOnlyProfileId" },
                    "Environment" : { "Ref" : "Environment"},
                    "ConfigBucket" : { "Ref": "ConfigBucket" },
                    "RootVolumeSize" : "100",
                    "NodeRole" : "nexus",
                    "PuppetMasterDNS" : { "Ref": "PuppetMasterDNS" }
                }
            }
        },
        "NexusNetworkInterface" : {
            "Type" : "AWS::EC2::NetworkInterface",
            "Properties" : {
                "Description": "The IP address of the nexus repository",
                "SourceDestCheck": "false",
                "GroupSet": [{ "Ref" : "NexusSecGroupId" }],
                "SubnetId": { "Ref" : "PrivateSubnetId"},
                "PrivateIpAddress": { "Ref" : "NexusPrivateIP"}
            }
        }
    },
    "Outputs" : {
        "NexusRepoUrl" : {
          "Value" : { "Fn::Join" : ["", ["http://", { "Ref" : "NexusPrivateIP"}, ":8081/nexus" ]] },
          "Description" : "The url to connect to the nexus repo that was just created, give time for puppet to run"
        }
    }
}
