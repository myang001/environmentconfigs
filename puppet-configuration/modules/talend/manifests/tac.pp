class talend::tac (
  $lifecycle      = 'dev',
  $baseURL        = 'http://10.100.100.1:8000/Software/talend/enterprise/5.2.2/manual',
  $baseName       = 'Talend-AdministrationCenter-r99271-V5.2.2',
  $tomcatBaseURL  = 'http://10.100.100.1/Software/apache-tomcat',
  $tomcatBaseName = 'apache-tomcat-7.0.39') {

  file { '/opt/tis/tac':
    ensure => directory,
    owner  => 'svctis',
    group  => 'svctis',
    mode   => 0777,
  }

  # first, download and setup tomcat
  exec { 'get-tomcat-source':
    command => "wget -c  ${tomcatBaseURL}/${tomcatBaseName}.tar.gz",
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/tac',
    creates => '/opt/tis/tac/apache-tomcat',
    require => File['/opt/tis/tac'],
    unless  => [
      "test -e /opt/tis/tac/${tomcatBaseName}.tar.gz",
    ],
  }

  # extract tomcat source
  exec { "tar zxf ${tomcatBaseName}.tar.gz && mv ${tomcatBaseName} apache-tomcat":
    alias   => 'extract-tomcat-source',
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/tac',
    creates => '/opt/tis/tac/apache-tomcat',
    require => Exec['get-tomcat-source'],
  }

  # clean up
  file { "/opt/tis/tac/${tomcatBaseName}.tar.gz":
    ensure  => absent,
    require => Exec['extract-tomcat-source'],
  }

  # download the file if we need to
  exec { 'get-tac-source':
    command => "wget -c ${baseURL}/${baseName}.zip",
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis',
    creates => '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator',
    require => Exec['extract-tomcat-source'],
    unless  => [
      "test -e /opt/tis/${baseName}.zip",
    ],
  }

  # extract and rename the directory
  exec { "unzip -q ${baseName}":
    alias   => 'unarchive-tac-source',
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis',
    creates => '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator',
    require => Exec['get-tac-source'],
    unless  => [
      "test -e /opt/tis/${baseName}",
    ],
  }

  # cleanup
  file { "/opt/tis/${baseName}.zip":
    ensure  => absent,
    require => Exec['unarchive-tac-source'],
  }

  # move things around
  exec { 'move-endorsed-dir':
    command => "mv ${baseName}/endorsed tac/apache-tomcat/",
    path    => [ '/bin', '/usr/bin' ],
    cwd     => '/opt/tis',
    user    => 'svctis',
    creates => '/opt/tis/tac/apache-tomcat/endorsed',
    require => Exec['unarchive-tac-source'],
  }

  # extract war file
  exec { 'extract-war-file':
    command => "unzip -q -d /opt/tis/tac/apache-tomcat/webapps/org.talend.administrator ${baseName}/org.talend.administrator.war",
    path    => [ '/bin', '/usr/bin' ],
    cwd     => '/opt/tis',
    user    => 'svctis',
    creates => '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator',
    require => Exec['unarchive-tac-source'],
  }

  # extract Talend Artifact Repository
  exec { 'extract-talend-artifact-repository':
    command => "unzip -q -d /opt/tis/tac ${baseName}/Talend-Artifact-Repository-V5.2.1.zip",
    path    => [ '/bin', '/usr/bin' ],
    cwd     => '/opt/tis',
    user    => 'svctis',
    creates => '/opt/tis/tac/Talend-Artifact-Repository-V5.2.1',
    require => Exec['unarchive-tac-source'],
  }

  # cleanup
  file { "/opt/tis/${baseName}":
    ensure  => absent,
    force => true,
    require => [
      Exec['extract-talend-artifact-repository'],
      Exec['extract-war-file'],
      Exec['move-endorsed-dir'],
    ],
  }

  # set environment var for CATALINA_PID
  exec { 'echo "export CATALINA_PID=/opt/tis/tac/catalina.pid" >> /home/svctis/.bashrc':
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    unless  => "grep 'export CATALINA_PID=/opt/tis/tac/catalina.pid' /home/svctis/.bashrc 2>/dev/null",
    require => Exec['unarchive-tac-source'],
  }

  # TODO: customize TAC
  $extra_dirs = [
    '/opt/tis/tac/archive',
    '/opt/tis/tac/archive/cmdline',
    '/opt/tis/tac/archive/components',
    '/opt/tis/tac/archive/jobs',
    '/opt/tis/tac/archive/logs',
    '/opt/tis/tac/archive/logs/executionLogs',
    '/opt/tis/tac/archive/soa',
    '/opt/tis/tac/audit',
    '/opt/tis/tac/audit/reports',
  ]

  file { $extra_dirs:
    ensure  => directory,
    owner   => 'svctis',
    group   => 'svctis',
    mode    => 0777,
    require => File['/opt/tis/tac'],
  }

  $conf_file = '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator/WEB-INF/classes/configuration.properties'
  # copy the correct config file
  exec { 'get-config-file':
    command => "rm -f ${conf_file} && wget -c ${baseURL}/configuration.properties.${lifecycle} -O ${conf_file}",
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator/WEB-INF/classes',
    require => Exec['extract-war-file'],
    unless  => [
      "grep -F \"lifecycle=${lifecycle}\" ${conf_file}",
    ],
  }


  # ensure that we have the license file
  exec { 'get-license-file':
    command => "rm -f ${baseURL}/install.txt && wget -c ${baseURL}/install.txt",
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/tac/apache-tomcat/webapps/org.talend.administrator/WEB-INF/classes',
    require => Exec['extract-war-file'],
    unless  => [
      "grep -E \"^license.key=\" /opt/tis/tac/apache-tomcat/webapps/org.talend.administrator/WEB-INF/classes/install.txt",
    ],
  }

  # EXECUTE
  # require => [ Exec['get-license-file'], Replaceline['businessLog.path'] ],
}
