node /^.*internal$/ {
  hiera_include('classes')
    package { 'ntp' : ensure => installed, }
    service { "puppet" : ensure => running, enable => true, }
}

