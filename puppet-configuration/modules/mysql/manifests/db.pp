# See README.md for details.
define mysql::db (
  $user,
  $password,
<<<<<<< HEAD
=======
  $dbname      = $name,
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
  $charset     = 'utf8',
  $collate     = 'utf8_general_ci',
  $host        = 'localhost',
  $grant       = 'ALL',
  $sql         = '',
  $enforce_sql = false,
  $ensure      = 'present'
) {
  #input validation
  validate_re($ensure, '^(present|absent)$',
  "${ensure} is not supported for ensure. Allowed values are 'present' and 'absent'.")
<<<<<<< HEAD
  $table = "${name}.*"

  include '::mysql::client'

  mysql_database { $name:
=======
  $table = "${dbname}.*"

  include '::mysql::client'

  $db_resource = {
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
    ensure   => $ensure,
    charset  => $charset,
    collate  => $collate,
    provider => 'mysql',
    require  => [ Class['mysql::server'], Class['mysql::client'] ],
<<<<<<< HEAD
    before   => Mysql_user["${user}@${host}"],
  }
=======
  }
  ensure_resource('mysql_database', $dbname, $db_resource)
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303

  $user_resource = {
    ensure        => $ensure,
    password_hash => mysql_password($password),
    provider      => 'mysql',
    require       => Class['mysql::server'],
  }
  ensure_resource('mysql_user', "${user}@${host}", $user_resource)

  if $ensure == 'present' {
    mysql_grant { "${user}@${host}/${table}":
      privileges => $grant,
      provider   => 'mysql',
      user       => "${user}@${host}",
      table      => $table,
<<<<<<< HEAD
      require    => [ Mysql_user["${user}@${host}"], Class['mysql::server'] ],
=======
      require    => [Mysql_database[$dbname], Mysql_user["${user}@${host}"], Class['mysql::server'] ],
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
    }

    $refresh = ! $enforce_sql

    if $sql {
<<<<<<< HEAD
      exec{ "${name}-import":
        command     => "/usr/bin/mysql ${name} < ${sql}",
=======
      exec{ "${dbname}-import":
        command     => "/usr/bin/mysql ${dbname} < ${sql}",
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
        logoutput   => true,
        environment => "HOME=${::root_home}",
        refreshonly => $refresh,
        require     => Mysql_grant["${user}@${host}/${table}"],
<<<<<<< HEAD
        subscribe   => Mysql_database[$name],
=======
        subscribe   => Mysql_database[$dbname],
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
      }
    }
  }
}
