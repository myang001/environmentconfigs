class talend::jobserver (
  $bucketName   = "genfare-deployment-artifacts/talend",
  $baseZip      = "Talend-Tools-Installer-r118616-V5.5.1-installer.zip",
  $baseUnzip    = "TalendTools-5.5.1-cdrom",
  $binFile      = "Talend-Tools-Installer-r118616-V5.5.1-linux64-installer.run",
  $licenseFile  = "license.lic",
  $tmpDir       = "/var/tmp/talend",
  $optionFile   = "jobserver.ini"
) {
  #  puppet:///modules/<module name>/<file name>
  exec { "talend-download":
    creates => "$tmpDir/$baseZip",
    command => "/usr/bin/aws s3 cp s3://$bucketName/$baseZip $tmpDir",
    timeout => 0,
    require => [Package['awscli'], File['root awscli-config']],
  }
  -> exec { "license-download":
    creates => "$tmpDir/$licenseFile",
    command => "/usr/bin/aws s3 cp s3://$bucketName/$licenseFile $tmpDir",
    timeout => 0,
    require => [Package['awscli'], File['root awscli-config']],
  }
  -> exec { "talend-unzip":
    creates => "$tmpDir/$baseUnzip",
    command => "/usr/bin/unzip -q $baseZip",
    cwd     => "$tmpDir"
  }
  -> file { "$tmpDir/options.ini":
    source => "puppet:///modules/talend/$optionFile"
  }
  -> exec { "talend-install":
    command => "$binFile --mode unattended --optionfile $tmpDir/$optionFile",
    cwd     => $tmpDir
  }
}
