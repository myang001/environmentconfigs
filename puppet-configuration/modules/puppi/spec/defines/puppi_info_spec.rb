require "#{File.join(File.dirname(__FILE__),'..','spec_helper.rb')}"

describe 'puppi::info' do

  let(:title) { 'puppi::info' }
  let(:node) { 'rspec.example42.com' }
  let(:params) {
    { 'name'         =>  'sample',
      'description'  =>  'Sample Info',
      'templatefile' =>  'puppi/info.erb',
      'run'          =>  'myownscript',
    }
  }

  describe 'Test puppi info step file creation' do
    it 'should create a puppi::info step file' do
      should contain_file('/etc/puppi/info/sample').with_ensure('present')
    end
    it 'should populate correctly the puppi::info step file' do
<<<<<<< HEAD
      should contain_file('/etc/puppi/info/sample').with_content(/myownscript/)
=======
      content = catalogue.resource('file', '/etc/puppi/info/sample').send(:parameters)[:content]
      content.should match(/myownscript/)
>>>>>>> dbd03dc818434c4dc123b989d62fa9a1d1669303
    end
  end

end
