{
    "AWSTemplateFormatVersion" : "2010-09-09",
    "Description": "Template to create any needed IAM roles for the account",

    "Resources" : {  
        "ReadOnlyRole": {
          "Type": "AWS::IAM::Role",
          "Properties": {
            "AssumeRolePolicyDocument": {
              "Statement": [{
                "Effect": "Allow",
                "Principal": { "Service": [ "ec2.amazonaws.com" ] },
              "Action": [ "sts:AssumeRole" ]
              }]
            },
            "Path": "/"
          }
        },
        "ReadOnlyPolicies": {
          "Type": "AWS::IAM::Policy",
          "Properties": {
            "PolicyName": "ReadOnlyRolePolicy",
            "PolicyDocument": {
              "Statement": [{
                "Effect": "Allow",
                "Action": [ 
                    "autoscaling:Describe*",
                    "cloudformation:DescribeStacks",
                    "cloudformation:DescribeStackEvents",
                    "cloudformation:DescribeStackResources",
                    "cloudformation:GetTemplate",
                    "cloudformation:List*",
                    "cloudwatch:Describe*",
                    "cloudwatch:Get*",
                    "cloudwatch:List*",
                    "ec2:Describe*",
                    "elasticache:Describe*",
                    "elasticloadbalancing:Describe*",
                    "iam:List*",
                    "iam:Get*",
                    "route53:Get*",
                    "route53:List*",
                    "rds:Describe*",
                    "rds:ListTagsForResource",
                    "s3:Get*",
                    "s3:List*",
                    "ses:Get*",
                    "ses:List*",
                    "sns:Get*",
                    "sns:List*",
                    "sqs:GetQueueAttributes",
                    "sqs:ListQueues",
                    "sqs:ReceiveMessage"
                ],
                "Resource": "*"
              }]
            },
            "Roles": [ { "Ref": "ReadOnlyRole" } ]
          }
        },
        "ReadOnlyProfile": {
          "Type": "AWS::IAM::InstanceProfile",
          "Properties": {
            "Path": "/",
            "Roles": [ { "Ref": "ReadOnlyRole" } ]
          }
        }
    },
    "Outputs" : {
        "ReadOnlyProfileId" : {
          "Value" : { "Ref": "ReadOnlyProfile" },
          "Description" : "The Id to be referenced in other templates for the ReadOnlyIAMProfile"
        }
    } 
}
