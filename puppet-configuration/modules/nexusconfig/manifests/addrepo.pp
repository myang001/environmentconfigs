class nexusconfig::addrepo ( $reponame, $repolocation, $work_home, $work_conf){

    exec { "download_mvnrepo":
        creates => $repolocation,
        command => "/usr/bin/python -c \"import boto;boto.connect_s3().get_bucket('genfare-deployment-artifacts').get_key('${reponame}.zip').get_contents_to_filename('/tmp/${reponame}.zip')\"; /usr/bin/unzip /tmp/${reponame}.zip -d ${work_home}",
        user    => 'nexus',
        require => File[$work_conf],
    }

    augeas { "nexus_add_repo_${reponame}":
        incl    => "${work_home}/nexus/conf/nexus.xml",
        context => "/files/${work_home}/nexus/conf/nexus.xml/nexusConfiguration/repositories",
        lens    => 'Xml.lns',
        changes => [
            "set repository[id/#text='${reponame}']/id/#text ${reponame}",
            "set repository[id/#text='${reponame}']/name/#text ${reponame}",
            "set repository[id/#text='${reponame}']/providerRole/#text org.sonatype.nexus.proxy.repository.Repository",
            "set repository[id/#text='${reponame}']/providerHint/#text maven2",
            "set repository[id/#text='${reponame}']/localStatus/#text IN_SERVICE",
            "set repository[id/#text='${reponame}']/notFoundCacheTTL/#text 1440",
            "set repository[id/#text='${reponame}']/userManaged/#text true",
            "set repository[id/#text='${reponame}']/exposed/#text true",
            "set repository[id/#text='${reponame}']/browseable/#text true",
            "set repository[id/#text='${reponame}']/writePolicy/#text READ_ONLY",
            "set repository[id/#text='${reponame}']/indexable/#text true",
            "set repository[id/#text='${reponame}']/searchable/#text true",
            "set repository[id/#text='${reponame}']/localStorage/provider/#text file",
            "set repository[id/#text='${reponame}']/localStorage/url/#text ${repolocation}",
            "set repository[id/#text='${reponame}']/externalConfiguration/repositoryPolicy/#text RELEASE",
            "set repository[id/#text='${nexusconfig::group_repo}']/id/#text ${nexusconfig::group_repo}",
            "set repository[id/#text='${nexusconfig::group_repo}']/name/#text ${nexusconfig::group_repo}",
            "set repository[id/#text='${nexusconfig::group_repo}']/providerRole/#text org.sonatype.nexus.proxy.repository.GroupRepository",
            "set repository[id/#text='${nexusconfig::group_repo}']/providerHint/#text maven2",
            "set repository[id/#text='${nexusconfig::group_repo}']/localStatus/#text IN_SERVICE",
            "set repository[id/#text='${nexusconfig::group_repo}']/notFoundCacheTTL/#text 15",
            "set repository[id/#text='${nexusconfig::group_repo}']/userManaged/#text true",
            "set repository[id/#text='${nexusconfig::group_repo}']/exposed/#text true",
            "set repository[id/#text='${nexusconfig::group_repo}']/browseable/#text true",
            "set repository[id/#text='${nexusconfig::group_repo}']/writePolicy/#text READ_ONLY",
            "set repository[id/#text='${nexusconfig::group_repo}']/indexable/#text true",
            "set repository[id/#text='${nexusconfig::group_repo}']/localStorage/provider/#text file",
            "set repository[id/#text='${nexusconfig::group_repo}']/externalConfiguration/memberRepositories/memberRepository[#text='central']/#text central",
            "set repository[id/#text='${nexusconfig::group_repo}']/externalConfiguration/memberRepositories/memberRepository[#text='thirdparty']/#text thirdparty",
            "set repository[id/#text='${nexusconfig::group_repo}']/externalConfiguration/memberRepositories/memberRepository[#text='releases']/#text releases",
            "set repository[id/#text='${nexusconfig::group_repo}']/externalConfiguration/memberRepositories/memberRepository[#text='snapshots']/#text snapshots",
            "set repository[id/#text='${nexusconfig::group_repo}']/externalConfiguration/memberRepositories/memberRepository[#text='${reponame}']/#text ${reponame}"
        ],
        require => File[$work_conf],
        onlyif  => "get /augeas//files/${work_home}/nexus/conf/nexus.xml/mtime > 0",
        notify  => Service['nexus']
    }
}
