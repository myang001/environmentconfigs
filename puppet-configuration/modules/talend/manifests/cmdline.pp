class talend::cmdline ($baseURL = 'http://10.100.100.1:8000/Software/talend/enterprise/5.2.2/manual', $baseName = 'Talend-Studio-r99271-V5.2.2') {
  file { '/opt/tis/cmdline':
    ensure => directory,
    owner  => 'svctis',
    group  => 'svctis',
  }

  # download the file if we need to
  exec { "get-studio-source":
    timeout => 3600,
    command => "wget -c $baseURL/$baseName.zip",
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/cmdline',
    creates => '/opt/tis/cmdline/studio',
    require => File['/opt/tis/cmdline'],
  }

  # extract the file and rename the directory
  exec { "unzip -q $baseName.zip && mv $baseName studio":
    alias   => 'studio-extract',
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/cmdline',
    creates => '/opt/tis/cmdline/studio',
    require => Exec['get-studio-source'],
  }

  # download the license file
  exec { "wget -c $baseURL/license":
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/cmdline/studio',
    creates => '/opt/tis/cmdline/studio/license',
    require => Exec['studio-extract'],
  }

  # download the start script
  exec { "wget -c $baseURL/start_cmdline.sh && chmod 0744 /opt/tis/cmdline/start_cmdline.sh":
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/cmdline',
    creates => '/opt/tis/cmdline/start_cmdline.sh',
    require => Exec['studio-extract'],
  }

  # download the stop script
  exec { "wget -c $baseURL/stop_cmdline.sh && chmod 0744 /opt/tis/cmdline/stop_cmdline.sh":
    path    => [ '/bin', '/usr/bin' ],
    user    => 'svctis',
    cwd     => '/opt/tis/cmdline',
    creates => '/opt/tis/cmdline/stop_cmdline.sh',
    require => Exec['studio-extract'],
  }

  # remove the zip file if it's still there
  file { "/opt/tis/cmdline/$baseName.zip":
    ensure  => absent,
    require => Exec['studio-extract'],
  }
}
