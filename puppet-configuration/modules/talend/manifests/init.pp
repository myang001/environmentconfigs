class talend {
  # we need to have the service account and groups defined
  group { 'talend':
    ensure => present,
    gid    => 1052,
  }
  user { 'talend':
    ensure     => present,
    uid        => 1052,
    gid        => 1052,
    shell      => '/bin/bash',
    home       => '/home/talend',
    managehome => true,
    comment    => 'Talend Integration System Service Account',
    require    => Group['talend'],
  }

  # we need to have the app directory defined
  file { '/opt/talend':
    ensure  => directory,
    group   => 'talend',
    owner   => 'talend',
    require => User['talend'],
  }

  class { '::epel': }
  -> package { "python-pip": }
  -> exec { "Fix pip on Fedora <= 17":       #Older fedora based systems don't have a pip symlink created by the package
  command => '/usr/bin/python-pip install -U pip',
  creates  => '/usr/bin/pip'
  }
  -> package { "awscli":
    ensure   => '1.3.11',
    provider => "pip"
  }
  -> file { "/home/talend/.aws":
    require => User['talend'],
    ensure => directory,
    owner => talend
  }
  -> file { 'awscli-config':
    path => '/home/talend/.aws/config',
    ensure => file,
    owner => talend,
    content => "[default]
    output = json
    region = us-east-1
    aws_access_key_id = AKIAIWKBYQKJHCGJ7LTQ
    aws_secret_access_key = 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"
  }
  -> file { "/root/.aws":
    ensure => directory
  }
  -> file { 'root awscli-config':
    path => '/root/.aws/config',
    ensure => file,
    content => "[default]
    output = json
    region = us-east-1
    aws_access_key_id = AKIAIWKBYQKJHCGJ7LTQ
    aws_secret_access_key = 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"
  }

  # these should be installed before java?
  package { 'zip':
    ensure => installed,
  }

  package { 'unzip':
    ensure => installed,
  }

  # telnet is required by cmdline, maybe others
  package { 'telnet':
    ensure => installed,
  }

  # we use wget
  package { 'wget':
    ensure => installed,
  }
}
