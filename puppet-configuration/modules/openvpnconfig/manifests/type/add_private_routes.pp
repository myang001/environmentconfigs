define openvpnconfig::type::add_private_routes {
    exec { "vpn_routing_${name}" :
        command => "${openvpnconfig::openvpn_root}/scripts/confdba --mod --key=vpn.server.routing.private_network.0 --value=${name}",
        unless  => "${openvpnconfig::openvpn_root}/scripts/confdba --get --key=vpn.server.routing.private_network.0 | grep ${name}"
    }
}
