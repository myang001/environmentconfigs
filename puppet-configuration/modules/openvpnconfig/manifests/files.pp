class openvpnconfig::files {

    file { '/usr/local/bin/google-authenticator' :
        source => 'puppet:///modules/openvpnconfig/google-authenticator',
        mode   => '0755',
    }

    file { '/lib/x86_64-linux-gnu/security/pam_google_authenticator.so' :
        source => 'puppet:///modules/openvpnconfig/pam_google_authenticator.so',
        mode   => '0755',
    }

    file { "${openvpnconfig::openvpn_root}/lib/openvpn-auth-pam.so" :
        source => 'puppet:///modules/openvpnconfig/openvpn-auth-pam.so',
        mode   => '0755',
    }
}