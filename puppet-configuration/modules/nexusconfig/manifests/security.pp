class nexusconfig::security ( $work_conf ){

    firewall { '100 allow puppetmaster access':
        port   => 8081,
        proto  => tcp,
        action => accept,
    }
    file { "${work_conf}/security.xml" :
        source                  => 'puppet:///modules/nexusconfig/security.xml',
        owner                   => 'nexus',
        group                   => 'nexus',
        mode                    => '0755',
        selinux_ignore_defaults => true,
        require                 => File[$work_conf],
        notify                  => Service['nexus'],
    }
    file { $work_conf :
        ensure                  => directory,
        owner                   => 'nexus',
        recurse                 => true,
        selinux_ignore_defaults => true,
        group                   => 'nexus',
    }
}