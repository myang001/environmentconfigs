define openvpnconfig::type::add_admin {
  
  exec { "admin_${name}" :
    command => "${openvpnconfig::openvpn_root}/scripts/sacli --user ${name} --key prop_superuser --value true UserPropPut"
  }
}