define usermgmt::type::removed_user {
  $username=$title
  user { $username :
    ensure     => absent,
    home       => "/home/${username}",
    managehome => true
  }
}
