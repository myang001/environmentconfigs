# genfact.rb

require 'json'

filename = "/root/puppet_facts.json"
if not File.exist?(filename)
   exit
end

parsed = JSON.load(File.new(filename))
parsed.default = Hash.new
parsed["Puppet"].each do |key, value|
   actual_value = value
   if value.is_a? Array
    actual_value = value.join(',')
   end
   Facter.add("genfact_" + key) do
     setcode do
       actual_value
     end
   end
end