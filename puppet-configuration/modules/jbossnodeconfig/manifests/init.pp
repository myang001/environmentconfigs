# Class: jbossnodeconfig
#
# This module manages jbossnodeconfig
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class jbossnodeconfig {


  $rds_jdbc = hiera('rds_jdbc')
  $rds_user = hiera('rds_user')
  $rds_pass = hiera('rds_pass')
  $aws_accessKeyId = hiera('aws_accessKeyId')
  $aws_secretKey = hiera('aws_secretKey')
  $aws_Environment = hiera('aws_Environment')
  $gfcp_version = hiera('gfcp_version')

  file { "/etc/motd":
    content => "This is a GenfareCloud JBoss node.  This system is under puppet control.  Do not make changes locally."
  }

  service { "iptables":
    enable => false,
    ensure => stopped
  }

  $awsreleases = {
    id => "aws-releases",
    url => "https://s3.amazonaws.com/genfare-deployment-artifacts/repositories/releases",
    username => "$aws_accessKeyId",
    password => "$aws_secretKey",
    releases => {enabled => true},
    snapshots => {enabled => false},
  }
  $awssnapshots = {
    id => "aws-snapshots",
    url => "https://s3.amazonaws.com/genfare-deployment-artifacts/repositories/snapshots",
    username => "$aws_accessKeyId",
    password => "$aws_secretKey",
    releases => {enabled => false},
    snapshots => {enabled => true},
  }

  $genfarenexus = {
    id       => "genfarenexus",
    url      => "http://repository.gfcp.io/nexus/content/groups/Genfare/",
    releases => {enabled => false},
    snapshots => {enabled => false},
  }

  class { "maven::maven": }
  -> maven::settings { 'maven-user-settings' :
    servers => [$genfarenexus, $awsreleases, $awssnapshots],
    repos   => [$genfarenexus, $awsreleases, $awssnapshots],
    user    => 'root'
  } -> Maven <| |>

  class { '::epel': }
  -> package { "python-pip": }
  -> package { "awscli": provider => "pip" }

  exec { "download jboss":
    creates => "/var/tmp/jboss-eap-6.2.0.zip",
    command => "/usr/bin/aws s3 cp s3://genfare-deployment-artifacts/jboss-eap-6.2.0.zip /var/tmp",
    timeout => 0,
    require => [Package['awscli']],
    before => Class['jboss::install']
  }

  package { 'java-1.7.0-openjdk-devel' : 
    ensure => installed 
  }
  -> class { 'jboss':
    install => 'source',
    install_source => 'file://tmp/jboss-eap-6.2.0.zip',
    created_dirname => 'jboss-eap-6.2',
    version => '7',
    bindaddr => '0.0.0.0',
  }
  -> file { "/opt/jboss/standalone/configuration/mgmt-users.properties":
    content => "admin=fef4a2d5399b026a4f9e26aa72447eb7",
    owner => jboss
  }
  -> maven { '/opt/jboss/modules/com/mysql/main/mysql-connector-java-5.1.29.jar':
    groupid => 'mysql',
    artifactid => 'mysql-connector-java',
    version => '5.1.29',
    packaging => 'jar',
    require => Class['maven::maven']
  }
  -> file { '/opt/jboss/modules/com/mysql/main/module.xml':
    owner => jboss,
    content => '<?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.0" name="com.mysql">
    <resources>
    <resource-root path="mysql-connector-java-5.1.29.jar"/>
    </resources>
    <dependencies>
    <module name="javax.api"/>
    <module name="javax.transaction.api"/>
    </dependencies>
    </module>'
  }
  -> maven { '/opt/jboss/modules/org/quartz-scheduler/main/quartz-1.8.6.jar':
    groupid => 'org.quartz-scheduler',
    artifactid => 'quartz',
    version => '1.8.6',
    packaging => 'jar',
    require => Class['maven::maven']
  }
  -> maven { '/opt/jboss/modules/org/quartz-scheduler/main/quartz-jboss-1.8.6.jar':
    groupid => 'org.quartz-scheduler',
    artifactid => 'quartz-jboss',
    version => '1.8.6',
    packaging => 'jar',
    require => Class['maven::maven']
  }
  -> file { '/opt/jboss/modules/org/quartz-scheduler/main/module.xml':
    owner => jboss,
    content => '<?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.0" name="org.quartz-scheduler">
    <resources>
    <resource-root path="quartz-1.8.6.jar"/>
    <resource-root path="quartz-jboss-1.8.6.jar"/>
    </resources>
    <dependencies>
    <module name="org.slf4j"/>
    </dependencies>
    </module>'
  }
  -> file { 'jboss-variables':
    path    => '/etc/default/jboss',
    ensure  => 'file',
    content => "JBOSS_CONFIG=standalone-ha.xml",
  }
  -> file { 'install-standalone-conf':
    path    => "${jboss::real_jboss_dir}/bin/standalone.conf",
    owner   => 'jboss',
    group   => 'jboss',
    mode    => '0755',
    content => template('jbossnodeconfig/standalone.conf.erb'),
  }
  -> exec { 'remove-jboss-cli':
    command => "/bin/rm ${jboss::real_jboss_dir}/bin/jboss.cli",
  }
  -> file { 'install-jboss-cli':
    path    => "${jboss::real_jboss_dir}/bin/jboss.cli",
    owner   => jboss,
    content => template('jbossnodeconfig/jboss.cli.erb'),
    ensure  => file,
  }
  -> exec { 'jboss-stop':
    command => '/sbin/service jboss stop',
    require => Service['jboss'],
  }
  -> exec { "wipe-standalone":
    command => '/bin/cp -f /opt/jboss/standalone/configuration/standalone_xml_history/standalone.initial.xml /opt/jboss/standalone/configuration/standalone.xml',
  }
  -> exec { 'jboss-start':
    command => '/sbin/service jboss start',
    require => Service['jboss'],
  }
  -> exec { 'run-jboss-cli':
    command => "/opt/jboss/bin/jboss-cli.sh --connect --user=admin --password=password --file=${jboss::real_jboss_dir}/bin/jboss.cli",
  }
  -> exec {'reload-check':
    onlyif    => "/opt/jboss/bin/jboss-cli.sh --connect --user=admin --password=password \":read-attribute(name=server-state)\" | grep reload-required",
    command   => '/sbin/service jboss restart',
    logoutput => true
  }

  file { "/opt/jboss/standalone/deployments":
    ensure => directory,
    mode => 777,
    require => Class['jboss::install']
  }

}
