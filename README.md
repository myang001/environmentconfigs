Environment Configurations
==========================

Overview
--------
The Amazon Web Services environment will be configured by two different mechanisms

###CloudFormations

<http://aws.amazon.com/cloudformation/>

This will be used to configure the creation of security groups, load balancers, S3 buckets, EC2 instances and the like.

It will also handle the bootstrapping of puppet on the EC2 instances, this includes any scripts, keys or other files needed to do so.

###Puppet

<http://puppetlabs.com/>

This will be used for managing the host after it is created by CloudFormations.

Such activites include:

*   User administration
*   JBoss configuration
*   Web Server configuration