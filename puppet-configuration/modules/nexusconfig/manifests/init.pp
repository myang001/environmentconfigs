class nexusconfig {

    $nexus_root = '/srv'
    $work_home = "${nexus_root}/sonatype-work"
    $work_conf = "${work_home}/nexus/conf"
    $jboss_repo =  'jboss-eap-6.2.0-maven-repository'
    $jboss_repo_loc = "${work_home}/jboss-eap-6.2.0.GA-maven-repository"
    $group_repo = 'Genfare'

    group{ 'nexus':
        ensure => present,
        system => true
    }
    user{ 'nexus':
        ensure => present,
        comment => 'Nexus user',
        gid     => 'nexus',
        home    => "${nexus_root}/nexus",
        shell   => '/bin/bash', 
        system  => true,
        require => Group['nexus'],
    }
    class{ 'nexus':
        version     => '2.7.1',
        nexus_user  => 'nexus',
        nexus_group => 'nexus',
        nexus_root  => $nexus_root,
        require     => User['nexus']
    }

    class{ 'nexusconfig::addrepo':
        reponame     => $jboss_repo, 
        repolocation => $jboss_repo_loc,
        work_home    => $work_home,
        work_conf    => $work_conf
    }

    class{ 'nexusconfig::security':
        work_conf    => $work_conf
    }
}
