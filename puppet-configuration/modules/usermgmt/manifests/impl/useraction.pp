class usermgmt::impl::useraction {

  $add_users = hiera('users')
  $remove_users = hiera('removeusers')

  if is_hash($add_users){
    $authed_user_list=keys($add_users)
    usermgmt::type::authed_user { $authed_user_list : userhash => $add_users }
  }

  if is_hash($remove_users) {
    $remove_user_list=keys($remove_users)
    usermgmt::type::removed_user { $remove_user_list : }
  }
}