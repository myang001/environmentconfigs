#!/bin/bash

usage() { echo "Usage: $0 -b bucketname -p puppetmaster -e environment -r hostrole" 1>&2; exit 1; }

while getopts ":b:p:e:r:" o; do
    case "${o}" in
        b)
            bucket=${OPTARG}
            ;;
        p)
            puppet=${OPTARG}
            ;;
        e)
            env=${OPTARG}
            ;;
        r)
            role=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${bucket}" ] || [ -z "${puppet}" ] || [ -z "${env}" ] || [ -z "${role}" ]; then
    usage
fi
yum update -y
yum install -y https://yum.puppetlabs.com/el/6/products/x86_64/puppetlabs-release-6-11.noarch.rpm
yum install -y puppet-3.7.1-1.el6.noarch ruby-devel gcc make rubygems
gem install json

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem').get_contents_to_filename('/var/lib/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem')"
chmod 640 /var/lib/puppet/ssl/private_keys/generic-hostcert.gfcp.io.pem
python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('config/puppet/ssl/ca/signed/generic-hostcert.gfcp.io.pem').get_contents_to_filename('/var/lib/puppet/ssl/certs/generic-hostcert.gfcp.io.pem')"
chmod 640 /var/lib/puppet/ssl/certs/generic-hostcert.gfcp.io.pem

python -c "import boto;boto.connect_s3().get_bucket('$bucket').get_key('/config/puppet/puppetclient.conf').get_contents_to_filename('/etc/puppet/puppet.conf')"
eval "sed -i 's/%PuppetMaster%/$puppet/' /etc/puppet/puppet.conf"
eval "sed -i 's/%PuppetEnvironment%/$env/' /etc/puppet/puppet.conf"

eval "echo '{ \"Puppet\" : { \"role\" : \"$role\"}}' > /root/puppet_facts.json"
puppet agent
