#!/bin/bash

usage() { echo "$0 -p puppetmaster<ip-172-31-254-230.ec2.internal> -e environment<dev> -k leylocation<~/myawskeypair -h host<54.84.250.51>" 1>&2; exit 1; }

while getopts ":p:e:k:h:" o; do
    case "${o}" in
        p)
            puppet=${OPTARG}
            ;;
        e)
            env=${OPTARG}
            ;;
        k)
            key=${OPTARG}
            ;;
        h)
            host=${OPTARG}
            ;;
        *)
            echo ${OPTARG}
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${puppet}" ] || [ -z "${env}" ] || [ -z "${key}" ] || [ -z "${host}" ]; then
    echo "$puppet $env $key $host"
    usage
fi

ssh -i $key root@$host wget https://apt.puppetlabs.com/puppetlabs-release-precise.deb
ssh -i $key root@$host dpkg -i puppetlabs-release-precise.deb
ssh -i $key root@$host apt-get update
ssh -i $key root@$host apt-get -y upgrade
ssh -i $key root@$host apt-get -y --force-yes install puppet=3.4.2-1puppetlabs1 puppet-common=3.4.2-1puppetlabs1

ssh -i $key root@$host "echo '{ \"Puppet\" : { \"role\" : \"openvpn\"}}' > /root/puppet_facts.json"

ssh -i $key root@$host "printf '[agent]\n\tserver=$puppet\n\tenvironment=$env' >> /etc/puppet/puppet.conf"

ssh -i $key root@$host puppet agent