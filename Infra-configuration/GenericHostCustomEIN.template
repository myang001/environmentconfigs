{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "Create an generic RHEL 6.3 64 bit instance with a custom NetworkInterface in a VPC",

  "Parameters" : {
    "InstanceType" : {
      "Description" : "EC2 instance type",
      "Type" : "String",
      "Default" : "m1.small",
      "AllowedValues" : [ "m1.small","m1.medium","m1.large"],
      "ConstraintDescription" : "must be a valid EC2 instance type."
    },
    "InstanceName" : {
      "Description" : "The name to be displayed in the AWS console",
      "Type" : "String",
      "Default" : "GenericHost"
    },
    "IamInstanceProfile" : {
      "Description" : "The IAM instance profile to be used for the instance",
      "Type" : "String",
      "Default" : ""
    },
    "NetworkInterface" : {
      "Description" : "An alternate network interface that has been created for a static ip address",
      "Type" : "String",
      "Default" : ""
    },
    "KeyName": {
        "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the Stack",
        "Type": "String",
        "Default" : "genfareaws201403",
        "MinLength": "1",
        "MaxLength": "255",
        "AllowedPattern" : "[\\x20-\\x7E]*",
        "ConstraintDescription" : "can contain only ASCII characters."
    },
    "Environment" : {
        "Description" : "The Environment that this template will be created in.",
        "Type" : "String",
        "Default" : "dev",
        "AllowedValues" : [ "dev", "qa", "production"]
    },
    "NodeRole" : {
        "Description" : "The role that the generic instance will provide so puppet knows to set it up in that manner",
        "Type" : "String"
    },
    "PuppetMasterDNS" : {
        "Description" : "The host address for the node to connect to as the puppet master",
        "Type" : "String"
    },
    "ConfigBucket" : {
        "Description" : "The bucket that contains the templates and other configuration artifacts",
        "Type" : "String",
        "Default" : "configuration-artifacts"
    },
    "RootVolumeSize" : {
        "Description" : "The filesystem size",
        "Type" : "Number",
        "Default" : "10"
    }
  },

    "Mappings" : {
        "RegionMap" : {
            "us-east-1"      : { "AMI" : "ami-09767460" },
            "us-west-1"      : { "AMI" : "ami-dc86eaec" },
            "us-west-2"      : { "AMI" : "ami-a68fb3e3" }
        }
    },

  "Resources" : {
    "RHELHost" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "KeyName" : { "Ref" : "KeyName" },
        "IamInstanceProfile": { "Ref": "IamInstanceProfile" },
        "InstanceType" : { "Ref" : "InstanceType" },
        "Tags": [
          { "Key" : "Name", "Value": { "Ref" : "InstanceName"} }
        ],
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/sda1",
            "Ebs" : { "VolumeSize" : { "Ref" : "RootVolumeSize" } }
          }
        ],
        "NetworkInterfaces" : [ { 
            "NetworkInterfaceId" : {"Ref" : "NetworkInterface"}, "DeviceIndex" : "0" } ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
          "#!/bin/bash\n",
          "python -c \"import boto;boto.connect_s3().get_bucket('",
          { "Ref": "ConfigBucket" },
          "').get_key('/config/scripts/puppetagent.sh').get_contents_to_filename('/root/puppetagent.sh')\" \n",
          "chmod 700 /root/puppetagent.sh \n",
          "/root/puppetagent.sh -b ",
          { "Ref": "ConfigBucket" },
          " -p ",
          { "Ref": "PuppetMasterDNS" },
          " -e ",
          { "Ref": "Environment" },
          " -r ",
          { "Ref": "NodeRole" },
          " >> /root/install.log 2>&1\n"]]}}
      }
    }
  },

  "Outputs" : {
    "InstanceId" : {
      "Value" : { "Ref" : "RHELHost" },
      "Description" : "Instance Id of newly created instance"
    }
  }
}