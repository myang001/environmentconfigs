class usermgmt {
    include usermgmt::impl::useraction

    class { 'sudo': }

    sudo::conf { 'admins':
        priority => 10,
        content  => "%admins ALL=(ALL) ALL",
    }

    sudo::conf { 'ec2-user':
        priority => 10,
        content  => "ec2-user ALL=(ALL) NOPASSWD: ALL",
    }

    group { 'admins' :
        ensure => present,
    }
}
